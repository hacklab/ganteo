# build stage
FROM python:3.11-slim-bookworm AS builder

# install PDM
RUN pip install pdm

# copy files
COPY pyproject.toml pdm.lock README.md /project/
COPY src/ /project/src

# install dependencies and project into the local packages directory
WORKDIR /project
RUN mkdir __pypackages__ && pdm sync --prod --no-editable


# run stage
FROM python:3.11-slim-bookworm

# retrieve packages from build stage
ENV PYTHONPATH=/project/pkgs
COPY --from=builder /project/__pypackages__/3.11/lib /project/pkgs

# retrieve executables
COPY --from=builder /project/__pypackages__/3.11/bin/* /bin/

# Set default configuration
COPY src/ganteo/assets/config.yaml /project/

# set command/entrypoint, adapt to fit your needs
WORKDIR /project
CMD ["ganteo", "-c", "config.yaml"]
