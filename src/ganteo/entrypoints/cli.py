"""Command line interface definition."""

import logging

import typer

from .. import services, version
from . import configure_logger, load_config

app = typer.Typer()


@app.command()
def main(
    verbose: bool = typer.Option(False, "--verbose", "-v"),
    config_path: str = typer.Option(
        "./config.yaml",
        "--config_path",
        "-c",
        help="configuration file path",
        envvar="GANTEO_CONFIG_PATH",
    ),
) -> None:
    """Gancio Operator."""
    configure_logger(verbose)
    log = logging.getLogger(__name__)

    log.info("Version info:\n" + version.version_info())

    try:
        config = load_config(config_path)
    except FileNotFoundError:
        log.error(
            f"Could not load the config.yaml file at {config_path}, please check it's there"
        )
        raise typer.Exit(code=1)

    telegram_bot = services.configure_telegram_bot(config)

    if not services.healthcheck(config, telegram_bot):
        raise typer.Exit(code=2)

    log.info("Start the Telegram message polling")
    telegram_bot.infinity_polling()


if __name__ == "__main__":  # pragma: no cover
    # E1120: As the arguments are passed through the function decorators instead of
    # during the function call, pylint get's confused.
    app()
