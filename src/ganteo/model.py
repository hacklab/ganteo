"""Define the data models of the program."""

from typing import List

from pydantic import BaseModel, Field


class TelegramChannel(BaseModel):
    """Configure the correlation of a channel and a Gancio collective."""

    id_: str
    tags: List[str] = Field(default_factory=list)
    places: List[str] = Field(default_factory=list)
