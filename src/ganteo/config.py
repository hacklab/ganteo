"""Define the configuration of the main program."""

import logging
import os
from typing import List

from goodconf import GoodConf

from .model import TelegramChannel

log = logging.getLogger(__name__)


class Config(GoodConf):
    """Configure the application."""

    telegram_bot_token: str
    telegram_gancio_channel: str
    telegram_forward_channels: List[TelegramChannel]

    class Config:
        """Define the default files to check."""

        default_files = [
            os.path.expanduser("~/.local/share/ganteo/config.yaml"),
            "config.yaml",
        ]
