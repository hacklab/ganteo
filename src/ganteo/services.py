"""Define all the orchestration functionality required by the program to work.

Classes and functions that connect the different domain model objects with the adapters
and handlers to achieve the program's purpose.
"""

import logging
import re
from typing import TYPE_CHECKING, List
from urllib.parse import unquote

import telebot

from .config import Config

if TYPE_CHECKING:
    from telebot.types import Message

    from .model import TelegramChannel


log = logging.getLogger(__name__)


def configure_telegram_bot(config: "Config") -> telebot.TeleBot:
    bot = telebot.TeleBot(config.telegram_bot_token)

    @bot.channel_post_handler(content_types=["text", "photo"])  # type: ignore
    def forward_message(message: "Message") -> None:
        log.info(
            f"Processing incomming message {message.id} on {message.chat.username}"
        )
        if (
            message.chat.type == "channel"
            and f"@{message.chat.username}" == config.telegram_gancio_channel
        ):
            log.info(message.caption or message.text)
            for channel in interested_channels(
                message, config.telegram_forward_channels
            ):
                log.info(
                    f"Forwarding message {message.id} from {config.telegram_gancio_channel} to {channel.id_}"
                )
                bot.forward_message(
                    channel.id_, config.telegram_gancio_channel, message.id
                )

    return bot


def healthcheck(config: "Config", bot: telebot.TeleBot) -> bool:
    """Check whether all the program components are working as expected."""
    log.debug("Running the healthchecks: ")
    log.debug("- Testing Telegram authentication")
    try:
        bot.get_me()
    except telebot.apihelper.ApiTelegramException as error:
        log.error("Can't connect to telegram, please check your token")
        return False

    log.debug("- Testing Telegram channel authorization")
    channel_ids_to_check = [
        f"@{channel.id_}" for channel in config.telegram_forward_channels
    ]
    channel_ids_to_check.append(f"@{config.telegram_gancio_channel}")
    for channel_id in channel_ids_to_check:
        log.debug(f"  - Testing Telegram channel {channel_id}")
        try:
            bot.get_chat(channel_id)
        except telebot.apihelper.ApiTelegramException as error:
            log.error(
                f"Can't connect to the telegram channel {channel_id}, please check that your bot has access"
            )
            return False

    log.info("All healthchecks passed :)")
    return True


def interested_channels(
    message: "Message", channels: List["TelegramChannel"]
) -> List["TelegramChannel"]:
    """List which channels are interested in the message."""
    log.debug(f"Checking which channels are interested in the message {message.id}")
    # Get message information
    message_tags = []
    message_place = None

    for caption in message.caption_entities:
        if caption.type != "text_link":
            continue
        if "tag" in caption.url:
            message_tags.append(unquote(re.sub(".*/tag/", "", caption.url)))
        elif "place" in caption.url:
            message_place = unquote(re.sub(".*/place/", "", caption.url))
    log.info(f"The message place is: {message_place}")
    log.info(f'The message tags are: {", ".join(message_tags)}')

    channels_to_return = [
        channel
        for channel in channels
        if (set(message_tags) & set(channel.tags)) or message_place in channel.places
    ]
    if len(channels_to_return) == 0:
        log.info("No matching channel was found")
    else:
        log.info(
            f'These channels are interested: {", ".join([channel.id_ for channel in channels_to_return])}'
        )

    return channels_to_return
