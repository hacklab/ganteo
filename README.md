# Ganteo

Python Gancio operator.

## Installing

```bash
pip install ganteo
```

To configure it you need to create a `config.yaml` file similar to [this one](src/ganteo/assets.config.yaml).

If you don't want your token to be saved in a file you can use the environment variable `TELEGRAM_BOT_TOKEN`.

### Using docker

- Until we publish the docker images build the docker with `make build-docker`
- Start the service with:

  ```bash
  docker run -v $(pwd)/config.yaml:/project/config.yaml ganteo
  ```

### Manually

To run it manually use:

```bash
ganteo
```

## Install the development environment

Install `pdm`:

```bash
pipx install pdm
```

Create the virtualenv and activate it:

```bash
mkvirtualenv -a ~/path_to_ganteo ganteo
workon virtualenv
```

Install the requirements:

```bash
make install
```

Check the `Makefile` for useful development actions such as:

- `make format`: run the fixers
